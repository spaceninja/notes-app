const express = require('express');
const dotenv = require('dotenv').config();
const logger = require('morgan');
const path = require('path');
const cookieParser = require('cookie-parser');

// create express app
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// configuring the database
const mongoose = require('mongoose');

mongoose.connect(process.env.DATABASE, { useNewUrlParser: true })
    .then(() => console.log('connected to the database'))
    .catch(err => console.error('error connecting to db: ', err.errmsg));

// define a simple route
app.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
  });

// other routes
require('./app/routes/note.routes.js')(app);

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});