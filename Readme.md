# EasyNotes Application

RESTful CRUD API for a simple note-taking application using NodeJD, ExpressJS, MongoDB and Mongoose.

## Steps to Setup

1. Install dependencies

```bash
npm install
```

2. Run Server

```bash
node server.js
```

You can browse the apis at <http://localhost:3000>

### Tutorial
Based on a tutorial on [The CalliCoder Blog](https://www.callicoder.com) - 

<https://www.callicoder.com/node-js-express-mongodb-restful-crud-api-tutorial/>